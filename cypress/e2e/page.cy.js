let discoverMovies5;

describe("Pagination tests", () => {
    before(() => {
        cy.request(
            `https://api.themoviedb.org/3/discover/movie?api_key=${Cypress.env(
                "TMDB_KEY"
            )}&language=en-US&include_adult=false&include_video=false&page=5`
        )
            .its("body")
            .then((response) => {
                discoverMovies5 = response.results;
            });
    });

    beforeEach(() => {
        cy.visit("/");
    });

    describe("Pagination", () => {
        it("correct selected pagination button", () => {
            cy.get(".MuiPagination-ul>li").each(($button, index) => {
                if (index == 2) {cy.get($button).find("a[aria-current='true']")}
                else {cy.get($button).find("a[aria-current='true']").should("not.exist")}
            });
        });

        it("correct movies in page", () => {
            cy.get("a[aria-label='Go to page 5']").click();
            cy.url().should("include", `/page5`);
            cy.get(".MuiCardHeader-root").should("have.length", 20);
            cy.get(".MuiCardHeader-content").each(($card, index) => {
                cy.wrap($card).find("p").contains(discoverMovies5[index].title);
            });
        });


    });

    describe("Navigation", () =>{
        it("show previous and next page", () => {
            cy.get("a[aria-label='Go to page 2']").click();
            cy.get("a[aria-label='Go to previous page']").click();
            cy.url().should("include", `/page1`);
            cy.get("a[aria-label='Go to next page']").click();
            cy.url().should("include", `/page2`);
        });


    });

});