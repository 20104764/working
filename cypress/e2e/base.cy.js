let discoverMovies;
let upComingMovies;
let topRatedMovies;

describe("Base tests", () => {
  before(() => {
    cy.request(
        `https://api.themoviedb.org/3/discover/movie?api_key=${Cypress.env(
            "TMDB_KEY"
        )}&language=en-US&include_adult=false&include_video=false&page=1`
    )
        .its("body")
        .then((response) => {
          discoverMovies = response.results;
        });

    cy.request(
        `https://api.themoviedb.org/3/movie/upcoming?api_key=${Cypress.env(
            "TMDB_KEY"
        )}&include_adult=false&language=en-US&page=1`
    )
        .its("body")
        .then((response) => {
          upComingMovies = response.results;
        });

    cy.request(
        `https://api.themoviedb.org/3/movie/top_rated?api_key=${Cypress.env(
            "TMDB_KEY"
        )}&language=en-US&page=1`
    )
        .its("body")
        .then((response) => {
          topRatedMovies = response.results;
        });
  });

  beforeEach(() => {
    cy.visit("/");
  });

  describe("The Discover Movies page", () => {
    it("displays the page header and 20 movies", () => {
      cy.get("h3").contains("Discover Movies");
      cy.get(".MuiCardHeader-root").should("have.length", 20);
    });

  });

  describe("UpComing Movies", () => {
    beforeEach(() => {
      cy.get("button").contains("Movies").click();
      cy.get("li").contains("Upcoming").click();
      cy.wait(500);
    });
    it("displays the page header and 20 movies", () => {
      cy.get("h3").contains("Upcoming Movies");
      cy.get(".MuiCardHeader-root").should("have.length", 20);
    });

    it("displays the correct movie titles", () => {
      cy.get(".MuiCardHeader-content").each(($card, index) => {
        cy.wrap($card).find("p").contains(upComingMovies[index].title);
      });
    });
  });

  describe("TopRated Movies", () => {
    beforeEach(() => {
      cy.get("button").contains("Movies").click();
      cy.get("li").contains("Top Rated").click();
      cy.wait(500);
    });
    it("displays the page header and 20 movies", () => {
      cy.get("h3").contains("Top Rated Movies");
      cy.get(".MuiCardHeader-root").should("have.length", 20);
    });

    it("displays the correct movie titles", () => {
      cy.get(".MuiCardHeader-content").each(($card, index) => {
        cy.wrap($card).find("p").contains(topRatedMovies[index].title);
      });
    });
  });



});