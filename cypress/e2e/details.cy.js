let discoverMovies;
let movie;
let casts;

describe("Movie details pgae tests", () => {
    before(() => {
        cy.request(
            `https://api.themoviedb.org/3/discover/movie?api_key=${Cypress.env(
                "TMDB_KEY"
            )}&language=en-US&include_adult=false&include_video=false&page=1`
        )
            .its("body") // Take the body of HTTP response from TMDB
            .then((response) => {
                discoverMovies = response.results;
            });
    });

    describe("List", () => {
        before(() => {
            cy.request(
                `https://api.themoviedb.org/3/movie/${discoverMovies[1].id}/credits?api_key=${Cypress.env(
                    "TMDB_KEY"
                )}&language=en-US`
            )
                .its("body")
                .then((response) => {
                    casts = response.cast;
                });
        });

        beforeEach(() => {
            cy.visit(`/movies/${discoverMovies[1].id}`);
        });

        it("Casts number", () => {
            cy.get(".MuiBox-root").should("have.length", casts.length + 1);
            cy.get(".MuiGrid-root>.MuiTypography-h5").contains("Top Billed Cast");
        })


        it("Name and character of the casts", () => {
            cy.get(".MuiCardContent-root").each(($card, index) => {
                cy.wrap($card).contains(casts[index].name);
                cy.wrap($card).contains(casts[index].character);
            });
        })
    });
});