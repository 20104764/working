import React,{useState} from 'react';
import { styled, alpha } from '@mui/material/styles';
import AppBar from '@mui/material/AppBar';
import Box from '@mui/material/Box';
import Toolbar from '@mui/material/Toolbar';
import Typography from '@mui/material/Typography';
import InputBase from '@mui/material/InputBase';
import SearchIcon from '@mui/icons-material/Search';
import IconButton from '@mui/material/IconButton';
import AccountCircle from '@mui/icons-material/AccountCircle';
import Dialog from '../Dialog';

const Search = styled('div')(({ theme }) => ({
    position: 'relative',
    borderRadius: theme.shape.borderRadius,
    backgroundColor: alpha(theme.palette.common.white, 0.15),
    '&:hover': {
        backgroundColor: alpha(theme.palette.common.white, 0.25),
    },
    marginRight: theme.spacing(2),
    marginLeft: 0,
    width: '100%',
    [theme.breakpoints.up('sm')]: {
        marginLeft: theme.spacing(3),
        width: 'auto',
    },
}));

const SearchIconWrapper = styled('div')(({ theme }) => ({
    padding: theme.spacing(0, 2),
    height: '100%',
    position: 'absolute',
    pointerEvents: 'none',
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
}));

const StyledInputBase = styled(InputBase)(({ theme }) => ({
    color: 'inherit',
    '& .MuiInputBase-input': {
        padding: theme.spacing(1, 1, 1, 0),
        paddingLeft: `calc(1em + ${theme.spacing(4)})`,
        transition: theme.transitions.create('width'),
        width: '100%',
        [theme.breakpoints.up('md')]: {
            width: '20ch',
        },
    },
}));


export default function PrimarySearchAppBar({ handleSearchChange }) {
    const [loginDialogOpen, setLoginDialogOpen] = useState(false);
    const [registrationDialogOpen, setRegistrationDialogOpen] = useState(false);
    const [userEmail, setUserEmail] = useState(''); // 新增用户邮箱状态

    const openLoginDialog = () => {
        setLoginDialogOpen(true);
    };

    const openRegistrationDialog = () => {
        setRegistrationDialogOpen(true);
    };

    // 定义关闭登录对话框的函数
    const closeLoginDialog = () => {
        setLoginDialogOpen(false);
    };

    const closeRegistrationDialog = () => {
        setRegistrationDialogOpen(false);
    };

    console.log(userEmail)

    return (
        <Box sx={{ flexGrow: 1 }}>
            <AppBar position="static">
                <Toolbar>
                    <Typography
                        variant="h6"
                        noWrap
                        component="div"
                        sx={{ display: { xs: 'none', sm: 'block' } }}
                    >
                        Search For Films
                    </Typography>
                    <Search>
                        <SearchIconWrapper>
                            <SearchIcon />
                        </SearchIconWrapper>
                        <StyledInputBase
                            placeholder="Search…"
                            inputProps={{ 'aria-label': 'search' }}
                            onChange={handleSearchChange}
                        />
                    </Search>
                    <Box sx={{ flexGrow: 1 }} />
                    <div style={{ display: 'flex', alignItems: 'center' }}>
                        {userEmail ? (
                            <Typography variant="body1" sx={{ marginRight: 2 }}>
                                {userEmail}
                            </Typography>
                        ) : (
                            <IconButton color="inherit" onClick={openLoginDialog}>
                                <AccountCircle />
                            </IconButton>
                        )}
                    </div>
                    <Dialog
                        open={loginDialogOpen}
                        onClose={() => setLoginDialogOpen(false)}
                        isRegistration={false}
                        // 传递回调函数来设置用户邮箱
                        setUserEmail={setUserEmail} // 传递 setUserEmail 到 AuthDialog
                    />
                </Toolbar>
            </AppBar>
        </Box>
    );
}